<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Sale;
use Symfony\Component\Console\Descriptor\Descriptor;

class SaleController extends Controller
{
    public function store(Request $request)
    { 
        $request->validate([
            'quantity' => 'required|numeric',
            'unit_cost' => 'required|numeric',
            'product_id' => 'required|numeric',
        ]);

        $products = Product::where('id', $request->input('product_id'))
            ->orderBy('id', 'DESC') // Assuming you meant DESC for descending order
            ->select('id', 'name', 'profit_margin', 'shipping_cost')
            ->first();
        $profitMargin = 1 - ($products->profit_margin) / 100;
        // Calculate the total cost
        $cost = $request->input('quantity') * $request->input('unit_cost');
        // Calculate the selling price
        $sellingPrice = number_format(($cost / $profitMargin) + $products->shipping_cost, 2);
        Sale::create([
            'quantity' => $request->input('quantity'),
            'unit_cost' => $request->input('unit_cost'),
            'selling_price' => $sellingPrice,
            'product_id' => $request->input('product_id'),
        ]);

        return redirect()->back()->with('success', 'Sale recorded successfully. ');
    }

    public function index()
    {
        $sales = Sale::with('product')->get();
        
        return response()->json($sales);
        // return view('sales.index', compact('sales'));
    }

    public function coffeeSales()
    {
        $products = Product::where('status', 'active')
            ->orderBy('id', 'DESC')
            ->select('id', 'name', 'profit_margin', 'shipping_cost')
            ->get()
            ->toArray();
        
        return view('coffee_sales', compact('products'));
    }
}
