<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Button extends Component
{
    public $type;
    public $class;

    public function __construct($type = 'submit', $class = '')
    {
        $this->type = $type;
        $this->class = $class;
    }

    public function render()
    {
        return view('components.button');
    }
}
