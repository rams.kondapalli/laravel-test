<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Select extends Component
{
    public $name;
    public $id;
    public $class;
    public $required;
    public $options;

    public function __construct($name, $id = null, $class = '', $required = false, $options = [])
    {
        $this->name = $name;
        $this->id = $id ?: $name;
        $this->class = $class;
        $this->required = $required;
        $this->options = $options;
    }

    public function render()
    {
        return view('components.select');
    }
}
