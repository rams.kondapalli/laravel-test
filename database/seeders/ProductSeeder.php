<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert([
            [
                'name' => 'Gold Coffee',
                'status' => 'active',
                'profit_margin' => 25,  
                'shipping_cost' => 10 // Assuming the shipping cost is £10
            ]
        ]);
        
    }
}
