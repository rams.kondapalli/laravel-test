<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('New ☕️ Sales') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>


                    @endif
                    <form method="POST" action="{{ route('sales.store') }}" id="sale-form">
                        @csrf
                        <div class="flex items-center justify-left bg-gray-100" style="padding:20px">
                            <div class="mb-4" style="margin-left: 30px;">
                                <label for="products" class="block text-sm font-medium text-gray-700">Products</label>
                                @include('select')
                            </div>
                            <div class="mb-4" style="margin-left: 30px;">
                                <label for="quantity" class="block text-sm font-medium text-gray-700">Quantity</label>
                                <x-input id="quantity" name="quantity" required type="number" />
                            </div>
                            <div class="mb-4" style="margin-left: 30px;">
                                <label for="unit_cost" class="block text-sm font-medium text-gray-700">Unit Cost</label>
                                <x-input id="unit_cost" name="unit_cost" required type="number" />
                            </div>

                            <div class="mb-4" style="margin-left: 30px;">
                                <label for="normal-input" class="block text-sm font-medium text-gray-700">Selling Price</label>
                                <div id="selling_price">-</div>
                            </div>
                            <div class="mb-4" style="margin-left: 30px;">
                                <x-button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" id="record-sale">
                                    Record Sale
                                </x-button>
                            </div>
                            <input type="hidden" id="product_id" name="product_id" required type="number" value="<?= $products[0]['id'] ?>" />
                            <input type="hidden" id="profit_margin" name="profit_margin" required type="number" value="<?= $products[0]['profit_margin'] ?>" />
                            <input type="hidden" id="shipping_cost" name="shipping_cost" required type="number" value="<?= $products[0]['shipping_cost'] ?>" />
                        </div>
                    </form>

                    <div class="mb-12" style="margin-left: 30px;">

                        <div class="container mx-auto mt-8">

                            <div class="mb-4 text-3xl text-gray-600 font-bold">
                                {{ __('Previous Sales') }}
                            </div>
                            <div id="sales-table"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script>
    function previousSaleData() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Your AJAX call
        $.ajax({
            url: '/sales-get',
            method: 'GET',
            success: function(data) {
                let headers = ['Product','Quantity', 'Unit Cost', 'Selling Price'];
                let rows = data.map(sales => [sales.product.name,sales.quantity, sales.unit_cost, sales.selling_price]);
                // Generate the table HTML dynamically
                let tableHtml = renderTable(headers, rows);
                // Update the DOM with the new table HTML
                $('#sales-table').html(tableHtml);
            }
        });

    }
    // rendar tabel

    function renderTable(headers, rows) {
        // Generate table headers
        let headersHtml = headers.map(header => `
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    ${header}
                </th>
            `).join('');

        // Generate table rows
        let rowsHtml = rows.map(row => `
                <tr>
                    ${row.map(cell => `
                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                            ${cell}
                        </td>
                    `).join('')}
                </tr>
            `).join('');

        // Combine headers and rows into the complete table HTML
        return `
                <div class="bg-white shadow-md rounded my-6">
                    <table class="min-w-full leading-normal">
                        <thead>
                            <tr>${headersHtml}</tr>
                        </thead>
                        <tbody>${rowsHtml}</tbody>
                    </table>
                </div>
            `;
    }

    //order submit ajax 

    function orderSubmit() {
        $('#sale-form').submit(function(e) {
            e.preventDefault(); // Prevent default form submission

            // Serialize form data
            let formData = $(this).serialize();

            // Send AJAX request
            $.ajax({
                url: $(this).attr('action'), // Form action URL
                method: $(this).attr('method'), // Form method
                data: formData,
                success: function(response) {
                    // Show success message
                    // Reset form if needed
                    $('#sale-form')[0].reset();
                    $("#selling_price").text("-");
                    previousSaleData();
                },
                error: function(xhr) {
                    // Handle error response
                    let errors = xhr.responseJSON.errors;
                    let errorMessage = '';
                    for (let field in errors) {
                        errorMessage += errors[field][0] + '<br>';
                    }
                    console.log(errorMessage)
                }
            });
        });
    }


    $(document).ready(function() {
        $('#quantity, #unit_cost').change(function() {

            var unitCost = parseFloat($("#unit_cost").val());
            var quantity = parseInt($("#quantity").val());
            var profitMargin = parseFloat($("#profit_margin").val());
            var shippingCost = parseFloat($("#shipping_cost").val());
            // Calculate selling price
            var sellingPrice = calculateSellingPrice(unitCost, quantity, profitMargin, shippingCost);
            if (!isNaN(unitCost) && !isNaN(quantity) && unitCost > 0 && quantity > 0) {
                $("#selling_price").text(sellingPrice.toFixed(2));
            }
        });
        // previous sale data loading
        previousSaleData();
        orderSubmit();
        $('#products').change(function() {
            $("#product_id").val($(this).val())
            $("#profit_margin").val($(this).find('option:selected').attr('profit_margin'))
            $("#shipping_cost").val($(this).find('option:selected').attr('shipping_cost'))

    // alert($(this).find('option:selected').attr('profit_margin'));
});


    })

    function calculateSellingPrice(unitCost, quantity, profitMargin, shippingCost) {
        // Convert profit margin percentage to decimal
        profitMargin = 1 - (parseFloat(profitMargin) / 100);
        // Calculate the total cost
        var cost = quantity * unitCost;
        // Calculate the selling price
        var sellingPrice = (cost / profitMargin) + parseFloat(shippingCost);
        return sellingPrice;
    }
</script>