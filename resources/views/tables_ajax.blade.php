    <x-table :headers="['Name', 'Email', 'Role']" :rows="[
        ['John Doe', 'john@example.com', 'Admin'],
        ['Jane Smith', 'jane@example.com', 'Editor'],
        ['David Johnson', 'david@example.com', 'User']
    ]" />