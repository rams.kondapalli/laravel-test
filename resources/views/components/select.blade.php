<select name="{{ $name }}" id="{{ $id }}" class="{{ $class }}">
    @foreach($options as $value => $attributes)
    <option value="{{ $value }}" profit_margin="{{ $attributes['profit_margin'] }}" shipping_cost="{{ $attributes['shipping_cost'] }}">
        {{ $attributes['label'] }}
    </option>
    @endforeach
</select>