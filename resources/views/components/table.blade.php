<table {{ $attributes->merge(['class' => 'table-auto w-full']) }}>
    <thead>
        <tr>
            @foreach($headers as $header)
                <th class="px-4 py-2">{{ $header }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($rows as $row)
            <tr>
                @foreach($row as $cell)
                    <td class="border px-4 py-2">{{ $cell }}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
