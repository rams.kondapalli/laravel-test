@php
$select_products = [];
foreach ($products as $product) {
$select_products[$product['id']] = [
'label' => $product['name'],
'profit_margin' => $product['profit_margin'],
'shipping_cost' => $product['shipping_cost']
];
}
@endphp


<x-select name="products" id="products" class="bg-gray-100 form-select mt-1  w-full h-10 w-100" :required="true" :options="$select_products" />